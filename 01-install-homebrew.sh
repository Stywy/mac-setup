#!/bin/sh
#  _                          _                                          _        _   
# | |__   ___  _ __ ___   ___| |__  _ __ _____      __     ___  ___ _ __(_)_ __  | |_ 
# | '_ \ / _ \| '_ ` _ \ / _ \ '_ \| '__/ _ \ \ /\ / /____/ __|/ __| '__| | '_ \ | __|
# | | | | (_) | | | | | |  __/ |_) | | |  __/\ V  V /_____\__ \ (__| |  | | |_) || |_ 
# |_| |_|\___/|_| |_| |_|\___|_.__/|_|  \___| \_/\_/      |___/\___|_|  |_| .__/  \__|
#                                                                         |_|    

which -s brew > /dev/null 2>&1
if [[ $? != 0 ]] ; then
    # Install Homebrew
    echo "$(tput setaf 1)homebrew is missing"
    echo "$(tput setaf 3)homebrew will be installed: "
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" && echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/stywen/.zprofile && eval "$(/opt/homebrew/bin/brew shellenv)" && echo "$(tput setaf 2)Installing homebrew was successful"

else
    echo "$(tput setaf 2)homebrew is installed"
    echo "$(tput setaf 2)homebrew will be updated"
    brew update
fi
