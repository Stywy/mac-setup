#!/bin/sh
#                     _                                                _ _ _                   _              _                     _       _   
#  __  _____ ___   __| | ___        ___ ___  _ __ ___   __ _ _ __   __| | (_) _ __   ___      | |_ ___   ___ | |      ___  ___ _ __(_)_ __ | |_ 
#  \ \/ / __/ _ \ / _` |/ _ \_____ / __/ _ \| '_ ` _ \ / _` | '_ \ / _` | | || '_ \ / _ \_____| __/ _ \ / _ \| |_____/ __|/ __| '__| | '_ \| __|
#   >  < (_| (_) | (_| |  __/_____| (_| (_) | | | | | | (_| | | | | (_| | | || | | |  __/_____| || (_) | (_) | |_____\__ \ (__| |  | | |_) | |_ 
#  /_/\_\___\___/ \__,_|\___|      \___\___/|_| |_| |_|\__,_|_| |_|\__,_|_|_||_| |_|\___|      \__\___/ \___/|_|     |___/\___|_|  |_| .__/ \__|
#                                                                                                                                    |_|        

OUTPUT=$(xcode-select -p 1>/dev/null;echo $?)
echo "$(tput setaf 3)DEBUG MESSAGE: ${OUTPUT}"

if [ $OUTPUT -eq 0 ]; then
  echo "$(tput setaf 2)xcode-command-tools are installed"
else
  echo "$(tput setaf 1)xcode-command-tools are missing"
  echo "xcode-command-tools will be installed: "
  xcode-select --install && echo "$(tput setaf 2)Installing xcode-commandline-tools was successful"
fi



