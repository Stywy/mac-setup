#!/bin/sh
#  _                                             _                                         _       _   
# | |__  _ __ _____      __     _ __   __ _  ___| | ____ _  __ _  ___       ___   ___ _ __(_)_ __ | |_ 
# | '_ \| '__/ _ \ \ /\ / /____| '_ \ / _` |/ __| |/ / _` |/ _` |/ _ \_____/ __| / __| '__| | '_ \| __|
# | |_) | | |  __/\ V  V /_____| |_) | (_| | (__|   < (_| | (_| |  __/_____\__ \| (__| |  | | |_) | |_ 
# |_.__/|_|  \___| \_/\_/      | .__/ \__,_|\___|_|\_\__,_|\__, |\___|     |___/ \___|_|  |_| .__/ \__|
#                              |_|                         |___/                            |_|             

homebrew_packages=("git" "wget" "curl" "neofetch" "btop" "gtop" "ansible" "mlocate" "tree" "wakeonlan")
for str in ${homebrew_packages[@]}; do
  brew install $str
done

homebrew_cask=("affinity-photo" "telegram-desktop" "whatsapp" "sublime-text" "remote-desktop-manager-free" "alacritty" "firefox" "warp" "balenaetcher" "transmission")
for str in ${homebrew_cask[@]}; do
  brew install --cask $str
done
